package dataAccess.managers;

import dataAccess.AccessedField;
import dataAccess.ManyToManyLink;
import dataAccess.ManyToOneLink;
import dataAccess.entities.BaseEntity;
import dataAccess.entities.Company;
import dataAccess.entities.Customer;
import dataAccess.entities.Developer;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public abstract class Manager<T> {

    public abstract String getTableName();

    public abstract Class<? extends BaseEntity> getBaseEntityClass();

    public abstract List<? extends BaseEntity<T>> getList(ResultSet rs);

    public abstract Optional<List<ManyToOneLink>> getListOfManyToOneLinks();

    public abstract Optional<List<ManyToManyLink>> getListOfManyToManyLinks();

    public List<AccessedField> getListOfFields(){
        List<AccessedField> list = new ArrayList<>();
        for (Field field : getBaseEntityClass().getDeclaredFields()) {
            list.add(new AccessedField(field));
        }
        return list;
    }

    public List<AccessedField> getListOfNonLinkedFields(){
        List<AccessedField> list = new ArrayList<>();
        for (Field field : getBaseEntityClass().getDeclaredFields()) {
            if (! field.getName().contains("id") && ! field.getName().contains("Id")) {
                list.add(new AccessedField(field));
            }
        }
        return list;
    }

    public List<Object> getListOfValues(BaseEntity element, List<AccessedField> fields) {
        List<Object> list = new ArrayList<>();
        for (AccessedField field : fields) {
            list.add(field.getValue(element));
        }
        return list;
    }

    public static <V extends BaseEntity> V getNewInstance(Class<V> c) {
        try {
            return (V)c.getConstructor().newInstance();
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static <V extends BaseEntity<V>> Manager<V> getManagerByClass(Class<V> c) {
            return getNewInstance(c).getManager();
    }
}
