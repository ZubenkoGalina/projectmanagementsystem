package dataAccess.managers;

import dataAccess.AccessedField;
import dataAccess.entities.BaseEntity;
import dataAccess.entities.ManyToManyTable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by g.zubenko on 09.03.2017.
 */
public abstract class ManyToManyTableManager {
    public abstract AccessedField getFirstId();
    public abstract AccessedField getSecondId();

    public abstract Manager getFirstManager();
    public abstract Manager getSecondManager();
    public abstract String getTableName();

    public abstract Constructor getConstructor();

    public static <V extends ManyToManyTable> V getNewInstance(Class<V> c) {
        try {
            return (V)c.getConstructor().newInstance();
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static <V extends ManyToManyTable> ManyToManyTableManager getManagerByClass(Class<V> c) {
        return getNewInstance(c).getManager();
    }
}
