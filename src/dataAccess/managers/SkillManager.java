package dataAccess.managers;

import dataAccess.ManyToManyLink;
import dataAccess.ManyToOneLink;
import dataAccess.entities.BaseEntity;
import dataAccess.entities.Skill;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class SkillManager extends Manager<Skill> {
    private static SkillManager obj;
    private SkillManager() {
    }

    public static SkillManager getObj(){
        if (obj==null){
            obj = new SkillManager();
        }
        return obj;
    }

    @Override
    public String getTableName() {
        return "skills";
    }

    @Override
    public Class<? extends BaseEntity> getBaseEntityClass() {
        return Skill.class;
    }

    @Override
    public List<Skill> getList(ResultSet rs) {
        List<Skill> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Skill obj = new Skill(rs.getInt("id"),rs.getString("name"));
                list.add(obj);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<List<ManyToOneLink>> getListOfManyToOneLinks() {
        return Optional.empty();
    }

    @Override
    public Optional<List<ManyToManyLink>> getListOfManyToManyLinks() {
        return Optional.empty();
    }
}
