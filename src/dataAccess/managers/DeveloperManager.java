package dataAccess.managers;

import dataAccess.AccessedField;
import dataAccess.ManyToManyLink;
import dataAccess.ManyToOneLink;
import dataAccess.entities.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class DeveloperManager extends Manager<Developer> {
    private static DeveloperManager obj;
    private DeveloperManager() {
    }

    public static DeveloperManager getObj(){
        if (obj==null){
            obj = new DeveloperManager();
        }
        return obj;
    }

    @Override
    public String getTableName() {
        return "developers";
    }

    @Override
    public List<Developer> getList(ResultSet rs) {
        List<Developer> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Developer obj = new Developer(
                        rs.getInt(   "id"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getInt(   "projectId"),
                        rs.getInt(   "salary"));
                list.add(obj);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Class<? extends BaseEntity> getBaseEntityClass() {
        return Developer.class;
    }

    @Override
    public Optional<List<ManyToOneLink>> getListOfManyToOneLinks() {
        List<ManyToOneLink> list = new ArrayList<>();
        try {
            list.add(new ManyToOneLink<Project>(new AccessedField(Developer.class.getDeclaredField("projectId")),
                    ProjectManager.getObj()));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
        return Optional.of(list);
    }

    @Override
    public Optional<List<ManyToManyLink>> getListOfManyToManyLinks() {
        List<ManyToManyLink> list = new ArrayList<>();
        try {
            list.add(new ManyToManyLink<Developer,Skill>(
                    DeveloperSkills.class.getDeclaredField("developerId"),
                    DeveloperSkills.class.getDeclaredField("skillId"),
                    DeveloperManager.getObj(),SkillManager.getObj()));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
        return Optional.of(list);
    }
}
