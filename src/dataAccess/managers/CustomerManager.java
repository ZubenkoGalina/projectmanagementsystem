package dataAccess.managers;

import dataAccess.ManyToManyLink;
import dataAccess.ManyToOneLink;
import dataAccess.entities.BaseEntity;
import dataAccess.entities.Company;
import dataAccess.entities.Customer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class CustomerManager extends Manager<Customer>{
    private static CustomerManager obj;
    private CustomerManager() {
    }

    public static CustomerManager getObj(){
        if (obj==null){
            obj = new CustomerManager();
        }
        return obj;
    }

    @Override
    public String getTableName() {
        return "customers";
    }

    @Override
    public Class<? extends BaseEntity> getBaseEntityClass() {
        return Customer.class;
    }

    @Override
    public List<Customer> getList(ResultSet rs) {
        List<Customer> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Customer obj = new Customer(rs.getInt("id"),rs.getString("name"));
                list.add(obj);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<List<ManyToOneLink>> getListOfManyToOneLinks() {
        return Optional.empty();
    }

    @Override
    public Optional<List<ManyToManyLink>> getListOfManyToManyLinks() {
        return Optional.empty();
    }
}
