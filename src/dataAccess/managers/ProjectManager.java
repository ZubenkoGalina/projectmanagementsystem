package dataAccess.managers;

import dataAccess.AccessedField;
import dataAccess.ManyToManyLink;
import dataAccess.ManyToOneLink;
import dataAccess.entities.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class ProjectManager extends Manager<Project> {
    private static ProjectManager obj;
    private ProjectManager() {
    }

    public static ProjectManager getObj(){
        if (obj==null){
            obj = new ProjectManager();
        }
        return obj;
    }

    @Override
    public String getTableName() {
        return "projects";
    }

    @Override
    public Class<? extends BaseEntity> getBaseEntityClass() {
        return Project.class;
    }

    @Override
    public List<Project> getList(ResultSet rs) {
        List<Project> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Project obj = new Project(
                        rs.getInt(   "id"),
                        rs.getString("name"),
                        rs.getInt(   "customerId"),
                        rs.getInt(   "companyId"),
                        rs.getInt(   "cost"));
                list.add(obj);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<List<ManyToOneLink>> getListOfManyToOneLinks() {
        List<ManyToOneLink> list = new ArrayList<>();

        try {
            list.add(new ManyToOneLink(new AccessedField(Project.class.getDeclaredField("customerId")), CustomerManager.getObj()));
            list.add(new ManyToOneLink(new AccessedField(Project.class.getDeclaredField("companyId")), CompanyManager.getObj()));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }

        return Optional.of(list);
    }

    @Override
    public Optional<List<ManyToManyLink>> getListOfManyToManyLinks() {
        return Optional.empty();
    }
}
