package dataAccess.managers;

import dataAccess.AccessedField;
import dataAccess.entities.Developer;
import dataAccess.entities.DeveloperSkills;
import dataAccess.entities.Skill;

import java.lang.reflect.Constructor;

/**
 * Created by g.zubenko on 09.03.2017.
 */
public class DeveloperSkillsManager extends ManyToManyTableManager{
    private static DeveloperSkillsManager obj;
    private DeveloperSkillsManager() {
    }

    public static DeveloperSkillsManager getObj(){
        if (obj==null){
            obj = new DeveloperSkillsManager();
        }
        return obj;
    }

    private Constructor constructor;
    private AccessedField firstField;
    private AccessedField secondField;

    @Override
    public AccessedField getFirstId() {
        if (firstField==null){
            try {
                firstField = new AccessedField(DeveloperSkills.class.getDeclaredField("developerId"));
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
        }
        return firstField;
    }

    @Override
    public AccessedField getSecondId() {
        if (secondField==null){
            try {
                secondField = new AccessedField(DeveloperSkills.class.getDeclaredField("skillId"));
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
        }
        return secondField;
    }

    @Override
    public Manager getFirstManager() {
        return Manager.getManagerByClass(Developer.class);
    }

    @Override
    public Manager getSecondManager() {
        return Manager.getManagerByClass(Skill.class);
    }

    @Override
    public String getTableName() {
        return "developerSkills";
    }

    @Override
    public Constructor getConstructor() {
        if (constructor==null){
            try {
                constructor = getClass().getDeclaredConstructor(int.class, int.class);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
        return constructor;
    }
}
