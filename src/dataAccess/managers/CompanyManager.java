package dataAccess.managers;

import dataAccess.ManyToManyLink;
import dataAccess.ManyToOneLink;
import dataAccess.entities.BaseEntity;
import dataAccess.entities.Company;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class CompanyManager extends Manager<Company> {
    private static CompanyManager obj;
    private CompanyManager() {
    }

    public static CompanyManager getObj(){
        if (obj==null){
            obj = new CompanyManager();
        }
        return obj;
    }

    @Override
    public String getTableName() {
        return "companies";
    }

    @Override
    public Class<Company> getBaseEntityClass() {
        return Company.class;
    }

    @Override
    public List<? extends BaseEntity<Company>> getList(ResultSet rs) {
        List<Company> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Company obj = new Company(rs.getInt("id"), rs.getString("name"));
                list.add(obj);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<List<ManyToOneLink>> getListOfManyToOneLinks() {
        return Optional.empty();
    }

    @Override
    public Optional<List<ManyToManyLink>> getListOfManyToManyLinks() {
        return Optional.empty();
    }
}
