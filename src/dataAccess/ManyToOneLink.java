package dataAccess;

import dataAccess.entities.BaseEntity;
import dataAccess.managers.Manager;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class ManyToOneLink<T> {
    private AccessedField field;
    private Manager<T> manager;

    public ManyToOneLink(AccessedField field, Manager<T> manager) {
        this.field = field;
        this.manager = manager;

        String fieldName = field.getField().getName();
        fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
    }

    public AccessedField getField() {
        return field;
    }

    public void setField(AccessedField field) {
        this.field = field;
    }

    public Manager<T> getManager() {
        return manager;
    }

    public void setManager(Manager<T> manager) {
        this.manager = manager;
    }
}
