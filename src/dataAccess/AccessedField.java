package dataAccess;

import dataAccess.entities.BaseEntity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class AccessedField {
    private Method getter;
    private Method setter;
    private Field field;

    public AccessedField(Field field) {
        this.field = field;

        String fieldName = field.getName();
        fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
        try {
            getter = field.getDeclaringClass().getMethod("get" + fieldName);
            setter = field.getDeclaringClass().getMethod("set" + fieldName, field.getType());
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString(){
        return field.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o.getClass() == AccessedField.class)) {
            if ((o.getClass() == Field.class)) {
                return field.equals(o);
            }
            {
                return false;
            }
        }

        AccessedField that = (AccessedField) o;

        return field.equals(that.field);
    }

    @Override
    public int hashCode() {
        return field.hashCode();
    }

    public Object getValue(BaseEntity element){
        try {
            return getter.invoke(element);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public void setValue(BaseEntity element, Object value){
        try {
            setter.invoke(element, value);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public Method getGetter() {
        return getter;
    }

    public void setGetter(Method getter) {
        this.getter = getter;
    }

    public Method getSetter() {
        return setter;
    }

    public void setSetter(Method setter) {
        this.setter = setter;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }
}
