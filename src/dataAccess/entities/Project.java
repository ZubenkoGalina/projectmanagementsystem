package dataAccess.entities;

import dataAccess.managers.DeveloperManager;
import dataAccess.managers.Manager;
import dataAccess.managers.ProjectManager;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by g.zubenko on 23.02.2017.
 */
public class Project extends BaseEntity<Project> {
    private String name;
    private int customerId;
    private int companyId;
    private int cost;

    public Project(String name, int customerId, int companyId, int cost) {
        this.name = name;
        this.customerId = customerId;
        this.companyId = companyId;
        this.cost = cost;
    }

    public Project(int id, String name, int customerId, int companyId, int cost) {
        this(name, customerId, companyId, cost);
        super.setId(id);
    }

    public Project() {
        name="";
    }

    /*@Override
    public String toString() {
        return name;
    }*/

    @Override
    public Manager<Project> getManager() {
        return ProjectManager.getObj();
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", customerId=" + customerId +
                ", companyId=" + companyId +
                ", cost=" + cost +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
