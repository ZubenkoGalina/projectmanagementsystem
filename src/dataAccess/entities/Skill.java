package dataAccess.entities;
import dataAccess.managers.Manager;
import dataAccess.managers.ProjectManager;
import dataAccess.managers.SkillManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by g.zubenko on 22.02.2017.
 */
public class Skill extends BaseEntity<Skill> {
    private String name;

    public Skill(String name) {
        this.name = name;
    }

    public Skill(int id, String name) {
        super(id);
        this.name = name;
    }

    public Skill() {
        name="";
    }

    @Override
    public Manager<Skill> getManager() {
        return SkillManager.getObj();
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
