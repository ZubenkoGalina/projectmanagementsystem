package dataAccess.entities;

import dataAccess.ManyToOneLink;
import dataAccess.managers.CustomerManager;
import dataAccess.managers.DeveloperManager;
import dataAccess.managers.Manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by g.zubenko on 23.02.2017.
 */
public class Developer extends BaseEntity<Developer> {
    private String firstName;
    private String lastName;
    private int projectId;
    private int salary;

    public Developer(String firstName, String lastName, int projectId, int salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.projectId = projectId;
        this.salary = salary;
    }

    public Developer(int id, String firstName, String lastName, int projectId, int salary) {
        this(firstName, lastName, projectId, salary);
        super.setId(id);
    }

    public Developer() {
        firstName="";
        lastName="";
    }

    @Override
    public Manager<Developer> getManager() {
        return DeveloperManager.getObj();
    }

    @Override
    public String toString() {
        return firstName+' '+lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
