package dataAccess.entities;

import dataAccess.ManyToOneLink;
import dataAccess.managers.CompanyManager;
import dataAccess.managers.Manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by g.zubenko on 23.02.2017.
 */
public class Company extends BaseEntity<Company> {
    private String name;

    public Company(String name) {
        this.name = name;
    }

    public Company(int id, String name) {
        super(id);
        this.name = name;
    }

    @Override
    public Manager<Company> getManager() {
        return CompanyManager.getObj();
    }

    public Company() {
        name="";
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
