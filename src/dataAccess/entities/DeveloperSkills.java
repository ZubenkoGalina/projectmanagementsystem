package dataAccess.entities;

import dataAccess.managers.DeveloperSkillsManager;
import dataAccess.managers.Manager;
import dataAccess.managers.ManyToManyTableManager;

import java.lang.reflect.Constructor;

/**
 * Created by g.zubenko on 23.02.2017.
 */
public class DeveloperSkills implements ManyToManyTable {
    private static Constructor constructor;
    private int developerId;
    private int skillId;

    public DeveloperSkills(int developerId, int skillId) {
        this.developerId = developerId;
        this.skillId = skillId;
    }

    public DeveloperSkills() {
    }

    /*@Override
    public String getTableName() {
        return "developerSkills";
    }

    @Override
    public List<DeveloperSkills> getList(ResultSet rs) {
        List<DeveloperSkills> list = new ArrayList<>();
        try {
            while (rs.next()) {
                DeveloperSkills obj = new DeveloperSkills(
                        rs.getInt("id"),
                        rs.getInt("developerId"),
                        rs.getInt("skillId"));
                list.add(obj);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

   /* @Override
    public String toString() {
        return "("+developerId+", "+skillId+")";
    }*/

    @Override
    public String toString() {
        return "DeveloperSkills{" +
                "developerId=" + developerId +
                ", skillId=" + skillId +
                '}';
    }

    public int getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(int developerId) {
        this.developerId = developerId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    @Override
    public int getFirstId() {
        return getDeveloperId();
    }

    @Override
    public int getSecondId() {
        return getSkillId();
    }

    @Override
    public ManyToManyTableManager getManager() {
        return DeveloperSkillsManager.getObj();
    }
}
