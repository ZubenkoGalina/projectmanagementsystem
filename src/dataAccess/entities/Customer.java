package dataAccess.entities;

import dataAccess.ManyToOneLink;
import dataAccess.managers.CompanyManager;
import dataAccess.managers.CustomerManager;
import dataAccess.managers.Manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by g.zubenko on 23.02.2017.
 */
public class Customer extends BaseEntity<Customer> {
    private String name;

    public Customer(String name) {
        this.name = name;
    }

    public Customer(int id, String name) {
        super(id);
        this.name = name;
    }

    public Customer() {
        name="";
    }

    @Override
    public Manager<Customer> getManager() {
        return CustomerManager.getObj();
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
