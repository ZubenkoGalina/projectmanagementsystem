package dataAccess.entities;

import dataAccess.managers.Manager;
import dataAccess.managers.ManyToManyTableManager;

import java.lang.reflect.Constructor;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public interface ManyToManyTable {
    int getFirstId();
    int getSecondId();

    ManyToManyTableManager getManager();
}
