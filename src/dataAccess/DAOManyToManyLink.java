package dataAccess;

import dataAccess.entities.BaseEntity;
import dataAccess.entities.ManyToManyTable;
import dataAccess.managers.ManyToManyTableManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class DAOManyToManyLink {
    public List getByFirstId(ManyToManyTableManager t){
        String query = "Select "+t.getFirstManager().getTableName()+".* from "+t.getTableName()+" join "
                +t.getFirstManager().getTableName()+" on "+
                t.getFirstId().getField().getName()+" = Id" ;
        DAO dao = new DAO();
        return dao.executeQuery(query, t.getFirstManager());
    }

    public List getBySecondtId(ManyToManyTableManager t, BaseEntity element){
        String query = "Select "+t.getSecondManager().getTableName()+".* from "+t.getTableName()+" join "
                +t.getSecondManager().getTableName()+" on "+
                t.getSecondId().getField().getName()+" = Id where "+t.getFirstId().getField().getName()+" = ?" ;
        DAO dao = new DAO();
        return dao.executeQuery(query, t.getSecondManager(), element.getId());
    }

    public List getNonExistentBySecondtId(ManyToManyTableManager t, BaseEntity element){
        DAO dao = new DAO();
        List list = getBySecondtId(t, element);
        List all = dao.read(t.getSecondManager());
        all.removeAll(list);

        return all;
    }

    public void add(int firstId, int secondId, ManyToManyTableManager manager){
        String query = "Insert into "+manager.getTableName()+"("+manager.getFirstId().getField().getName()+
                ", "+manager.getSecondId().getField().getName()+") values(?,?)";
        List<Object> values = new ArrayList<>();
        values.add(firstId);
        values.add(secondId);

        DAO dao = new DAO();
        dao.execute(query, values);
    }

    public void delete(int firstId, int secondId, ManyToManyTableManager manager){
        String query = "Delete from "+manager.getTableName()+" where "+
                manager.getFirstId().getField().getName()+"=? and "+
                manager.getSecondId().getField().getName()+"=?";
        List<Object> values = new ArrayList<>();
        values.add(firstId);
        values.add(secondId);

        DAO dao = new DAO();
        dao.execute(query, values);
    }
}
