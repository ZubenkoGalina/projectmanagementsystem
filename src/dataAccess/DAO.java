package dataAccess;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import dataAccess.entities.BaseEntity;
import dataAccess.managers.Manager;

import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

/**
 * Created by g.zubenko on 22.02.2017.
 */
public final class DAO {
    public enum CRUDOperations {
        CREATE("create new element"), READ("read all elements"), UPDATE("update an element"), DELETE("delete an element");
        public final String msg;

        CRUDOperations(String msg) {
            this.msg = msg;
        }
    }

    static private MysqlConnectionPoolDataSource dataSource;

    static {
        String DATABASE_URL = "jdbc:mysql://localhost/projects_db";
        String USER = "root";
        String PASSWORD = "";
        dataSource = new MysqlConnectionPoolDataSource();
        dataSource.setPassword(PASSWORD);
        dataSource.setUser(USER);
        dataSource.setURL(DATABASE_URL);
    }

    public <T> void create(BaseEntity<T> entity){
        List<AccessedField> fields = entity.getManager().getListOfFields();
        String fieldNames = fields.get(0).getField().getName();
        List<Object> values = entity.getManager().getListOfValues(entity, fields);
        String valuesString = "?";
        String queryForReturnId = "Select Id from "+entity.getManager().getTableName()+" where "+
                fields.get(0).getField().getName()+" = ?";
        for (int i=1; i<values.size();i++) {
            fieldNames+=", "+fields.get(i).getField().getName();
            valuesString+=", ?";
            queryForReturnId+= " and "+fields.get(i).getField().getName()+" = ?";
        }

        String query = "Insert into "+entity.getManager().getTableName()+"("+fieldNames+") Values("+valuesString+")";
        execute(query, values);

        entity.setId(executeQuery(queryForReturnId,values));
    }

    public <T> List<? extends BaseEntity<T>> read(Manager<T> manager) {
        String query = "Select * from " + manager.getTableName();
        return executeQuery(query, manager);
    }

     public <T> void update(BaseEntity<T> entity, List<AccessedField> fields){
        String query = "Update "+entity.getManager().getTableName();

        for (int i=0; i<fields.size(); i++){
            if (i==0) {query+=" Set ";} else {query+=", ";}
            query+= fields.get(i).getField().getName()+" = ?";
        }

        query+=" where id = ?";
        execute(query,entity.getManager().getListOfValues(entity,fields),entity.getId());
    }

    public <T> void delete(BaseEntity<T> entity){
        execute("Delete from "+entity.getManager().getTableName()+" where id = ?",entity.getId());
    }

    private void execute(String query) {
        try (Connection con = dataSource.getConnection();
             Statement stmt = con.createStatement()){
            stmt.execute(query);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    private void execute(String query, int id) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement stmt = con.prepareStatement(query)){
            stmt.setInt(1,id);
            stmt.execute();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public void execute(String query, List<Object> values, int id) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement stmt = con.prepareStatement(query)) {
            setValues(values, stmt);
            stmt.setInt(values.size() + 1, id);
            stmt.execute();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public void execute(String query, List<Object> values) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement stmt = con.prepareStatement(query)) {
            setValues(values, stmt);
            stmt.execute();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    private void setValues(List<Object> values, PreparedStatement stmt) {
        for (int i = 0; i < values.size(); i++) {
            Object value = values.get(i);
            try {
                if (value.getClass() == String.class) {
                    stmt.setString(i + 1, (String) value);
                } else if (value.getClass() == Integer.class) {
                    stmt.setInt(i + 1, (Integer) value);
                } else {
                    throw new RuntimeException("Unsupported field type " + value.getClass().getSimpleName());
                }
            } catch (SQLException e) {
                throw new RuntimeException("Unsupported field type " + value.getClass().getSimpleName(), e);
            }
        }
    }

    public <T> List<? extends BaseEntity<T>> executeQuery(String query, Manager<T> manager) {
        try (Connection con = dataSource.getConnection();
             Statement stmt = con.createStatement()){
            return manager.getList(stmt.executeQuery(query));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int executeQuery(String query, List<Object> values) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement stmt = con.prepareStatement(query)) {
            setValues(values, stmt);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getInt("Id");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> List<? extends BaseEntity<T>> executeQuery(String query, Manager<T> manager, int id) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement stmt = con.prepareStatement(query)){
            stmt.setInt(1,id);
            return manager.getList(stmt.executeQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
