package dataAccess;

import dataAccess.entities.BaseEntity;
import dataAccess.entities.ManyToManyTable;
import dataAccess.managers.Manager;
import dataAccess.managers.ManyToManyTableManager;

import java.lang.reflect.Field;

/**
 * Created by g.zubenko on 07.03.2017.
 */
public class ManyToManyLink<T extends BaseEntity<T> ,K extends BaseEntity<K>> {
    private Manager<T> firstManager;
    private Manager<K> secondManager;
    private AccessedField firstField;
    private AccessedField secondField;
    private ManyToManyTableManager manyToManyTableManager;

    public ManyToManyLink(Field firstField, Field secondField, Manager<T> firstManager, Manager<K> secondManager) {
        this.firstField = new AccessedField(firstField);
        this.secondField = new AccessedField(secondField);

        Class manyToManyTable = firstField.getDeclaringClass();
        manyToManyTableManager = ManyToManyTableManager.getManagerByClass(manyToManyTable);

        this.firstManager = firstManager;
        this.secondManager = secondManager;
    }

    public Manager<T> getFirstManager() {
        return firstManager;
    }

    public void setFirstManager(Manager<T> firstManager) {
        this.firstManager = firstManager;
    }

    public Manager<K> getSecondManager() {
        return secondManager;
    }

    public void setSecondManager(Manager<K> secondManager) {
        this.secondManager = secondManager;
    }

    public AccessedField getFirstField() {
        return firstField;
    }

    public void setFirstField(AccessedField firstField) {
        this.firstField = firstField;
    }

    public AccessedField getSecondField() {
        return secondField;
    }

    public void setSecondField(AccessedField secondField) {
        this.secondField = secondField;
    }

    public ManyToManyTableManager getManyToManyTableManager() {
        return manyToManyTableManager;
    }

    public void setManyToManyTableManager(ManyToManyTableManager manyToManyTableManager) {
        this.manyToManyTableManager = manyToManyTableManager;
    }
}
