package GUI;

import dataAccess.DAO;
import dataAccess.entities.*;
import dataAccess.entities.BaseEntity;
import dataAccess.managers.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by g.zubenko on 22.02.2017.
 */
public class Main {
    public static void main(String[] args) {
        boolean con = true;
        while (con) {
            System.out.println("With what table do you want to work?");
            List<Manager<? extends BaseEntity>> tableList = new ArrayList<>();
            tableList.add(CompanyManager.getObj());
            tableList.add(CustomerManager.getObj());
            tableList.add(DeveloperManager.getObj());
            tableList.add(ProjectManager.getObj());
            tableList.add(SkillManager.getObj());
        //    tableList.add(new DeveloperSkills());

            for (int i = 0; i < tableList.size(); i++) {
                System.out.println((i + 1) + ". " + tableList.get(i).getTableName());
            }

            int tableNumber = IOUtil.readInt("table number", 1, tableList.size()) - 1;

            List<DAO.CRUDOperations> operationList = new ArrayList<>();
            Collections.addAll(operationList, DAO.CRUDOperations.values());

            System.out.println("What operation do you want to execute?");
            for (int i = 0; i < operationList.size(); i++) {
                System.out.println((i + 1) + ". " + operationList.get(i).msg);
            }

            DAO.CRUDOperations operation = operationList.get(IOUtil.readInt("operation number", 1, operationList.size()) - 1);

            Menu m = new Menu();
            m.run(operation,tableList.get(tableNumber));

            con = IOUtil.askToContinue("Do you want to continue?");
        }
    }
}