package GUI;

import dataAccess.*;
import dataAccess.entities.BaseEntity;
import dataAccess.entities.ManyToManyTable;
import dataAccess.managers.Manager;
import dataAccess.managers.ManyToManyTableManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by g.zubenko on 27.02.2017.
 */
class Menu {

     <T> void run(DAO.CRUDOperations operation, Manager<T> manager){
        if (operation == DAO.CRUDOperations.READ) {read(manager);}
        if (operation == DAO.CRUDOperations.DELETE) {delete(manager);}
        if (operation == DAO.CRUDOperations.UPDATE) {update(manager);}
        if (operation == DAO.CRUDOperations.CREATE) {create(manager);}
    }

    private <T> void read(Manager<T> manager) {
            DAO dao = new DAO();
            IOUtil.printCollection(manager.getTableName(), dao.read(manager), false);
    }

    private <T> void delete(Manager<T> manager) {
            DAO dao = new DAO();

            List<? extends BaseEntity<T>> elements = dao.read(manager);
            IOUtil.printCollection(manager.getTableName(), elements, false);
            int elementNumber = IOUtil.readInt("number of element you want to delete", 1, elements.size()) - 1;
            dao.delete(elements.get(elementNumber));

    }

    private <T> void create(Manager<T> manager) {
        BaseEntity element = Manager.getNewInstance(manager.getBaseEntityClass());

        for (AccessedField field : manager.getListOfNonLinkedFields()) {
            if (field.getField().getType() == String.class) {
                field.setValue(element, IOUtil.readString(field.getField().getName(), false));
            }
            if (field.getField().getType() == int.class) {
                field.setValue(element, IOUtil.readInt(field.getField().getName(), false, false));
            }
        }

        DAO dao = new DAO();

        Optional<List<ManyToOneLink>> linksManyToOne = element.getManager().getListOfManyToOneLinks();
        if (linksManyToOne.isPresent()){
            for (ManyToOneLink link:linksManyToOne.get()){
                List<BaseEntity> items = dao.read(link.getManager());
                IOUtil.printCollection(link.getManager().getTableName(),items,false);
                BaseEntity entity = items.get(IOUtil.readInt(
                        link.getManager().getBaseEntityClass().getSimpleName().toLowerCase()+" number",1,items.size())-1);
                link.getField().setValue(element,entity.getId());
            }
        }

        dao.create(element);

        Optional<List<ManyToManyLink>> linksManyToMany = element.getManager().getListOfManyToManyLinks();
        if (linksManyToMany.isPresent()){
            for (ManyToManyLink link:linksManyToMany.get()) {
                if (IOUtil.askToContinue("Would you like to add " + link.getSecondManager().getTableName() + "?")) {
                        DAOManyToManyLink daoMTM = new DAOManyToManyLink();
                        String fieldName = link.getSecondField().getField().getName();
                        fieldName = fieldName.substring(0, fieldName.length() - 2);

                        do {
                            List<BaseEntity> allowedItemsToAdd = daoMTM.getNonExistentBySecondtId(link.getManyToManyTableManager(), element);
                            IOUtil.printCollection(link.getSecondManager().getTableName() + " to add", allowedItemsToAdd,false);
                            if (allowedItemsToAdd.size() == 0) {
                                break;
                            }
                            if (allowedItemsToAdd.size() > 0) {
                                int num = IOUtil.readInt("number of " + fieldName, 1, allowedItemsToAdd.size()) - 1;

                                daoMTM.add(element.getId(), allowedItemsToAdd.get(num).getId(), link.getManyToManyTableManager());
                            }
                        }
                        while (IOUtil.askToContinue("Would you like to add one more " + fieldName + "?"));
                }
            }
        }
    }

    private <T> void update(Manager<T> manager) {
        DAO dao = new DAO();
        String tableName = manager.getTableName();

        List<? extends BaseEntity> elements = dao.read(manager);
        IOUtil.printCollection(tableName, elements, false);
        int elementNumber = IOUtil.readInt("number of element you want to update", 1, elements.size()) - 1;
        BaseEntity<T> element = elements.get(elementNumber);

        List<AccessedField> updatedFields = new ArrayList<>();
        for (AccessedField field : manager.getListOfNonLinkedFields()) {
            try{
            if (field.getField().getType() == String.class) {
                field.setValue(element, IOUtil.readString("new "+field.getField().getName(), true, true));
            }
            if (field.getField().getType() == int.class) {
                field.setValue(element, IOUtil.readInt("new "+field.getField().getName(), true, true));
            }
            updatedFields.add(field);
            }
            catch (InputWasSkippedException e){ }
        }

        Optional<List<ManyToOneLink>> linksManyToOne = element.getManager().getListOfManyToOneLinks();
        if (linksManyToOne.isPresent()) {
            for (ManyToOneLink link : linksManyToOne.get()) {
                try {
                    List<BaseEntity> items = dao.read(link.getManager());
                    IOUtil.printCollection(link.getManager().getTableName(), items, false);
                    BaseEntity entity = items.get(IOUtil.readInt("new "+
                            link.getManager().getBaseEntityClass().getSimpleName().toLowerCase() + " number", 1, items.size(), true) - 1);
                    link.getField().setValue(element, entity.getId());
                    updatedFields.add(link.getField());
                } catch (InputWasSkippedException e) {
                }
            }
        }

        Optional<List<ManyToManyLink>> linksManyToMany = element.getManager().getListOfManyToManyLinks();
        if (linksManyToMany.isPresent()){
            for (ManyToManyLink link:linksManyToMany.get()) {
                if (IOUtil.askToContinue("Would you like to edit " + link.getSecondManager().getTableName() + "?")) {
                    System.out.println("1 - add\n2 - delete\n3 - continue");
                    int numberOfAction = IOUtil.readInt("number of action", 1, 3);
                    while (numberOfAction != 3) {
                        DAOManyToManyLink daoMTM = new DAOManyToManyLink();
                        String fieldName = link.getSecondField().getField().getName();
                        fieldName = fieldName.substring(0, fieldName.length() - 2);
                        if (numberOfAction == 1) {
                            do {
                                List<BaseEntity> allowedItemsToAdd = daoMTM.getNonExistentBySecondtId(link.getManyToManyTableManager(), element);
                                if (allowedItemsToAdd.size() == 0) {
                                    break;
                                }
                                IOUtil.printCollection(link.getSecondManager().getTableName() + " to add", allowedItemsToAdd, false);
                                if (allowedItemsToAdd.size() > 0) {
                                    int num = IOUtil.readInt("number of " + fieldName, 1, allowedItemsToAdd.size()) - 1;

                                    daoMTM.add(element.getId(), allowedItemsToAdd.get(num).getId(), link.getManyToManyTableManager());
                                }
                            } while (IOUtil.askToContinue("Would you like to add one more " + fieldName + "?"));
                        }
                        if (numberOfAction == 2) {
                            do {
                                List<BaseEntity> allowedItemsToAdd = daoMTM.getBySecondtId(link.getManyToManyTableManager(), element);
                                IOUtil.printCollection(link.getSecondManager().getTableName() + " to delete", allowedItemsToAdd, false);
                                if (allowedItemsToAdd.size() == 0) {
                                    break;
                                }
                                if (allowedItemsToAdd.size() > 0) {
                                    int num = IOUtil.readInt("number of " + fieldName, 1, allowedItemsToAdd.size()) - 1;

                                    daoMTM.delete(element.getId(), allowedItemsToAdd.get(num).getId(), link.getManyToManyTableManager());
                                }
                            } while (IOUtil.askToContinue("Would you like to delete one more " + fieldName + "?"));
                        }

                        System.out.println("1 - add\n2 - delete\n3 - continue");
                        numberOfAction = IOUtil.readInt("number of action", 1, 3);
                    }
                }
            }
        }

        if (updatedFields.size()>0) {
            dao.update(element, updatedFields);
        }
    }
}

