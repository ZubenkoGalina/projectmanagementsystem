package GUI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by g.zubenko on 26.01.2017.
 */
public final class IOUtil {
    static final private String IOExceptionMsg = "Input failed.";
    static final private BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

    static public void pressEnterToContinue() {
        System.out.print("Press enter to continue...");
        try {
            consoleReader.readLine();
        } catch (IOException e) {
            System.out.println(IOExceptionMsg);
        }
    }

    static public String readString(String stringDescription, boolean mayBeEmpty){
        return readString(stringDescription, mayBeEmpty, !mayBeEmpty);
    }

    static public String readString(String stringDescription, boolean mayBeEmpty, boolean informUser){
        System.out.print("Input "+stringDescription+
                ((mayBeEmpty && informUser)?" or press enter to skip this step":"")+
                ": ");
        String value = "";
        try {
            value = consoleReader.readLine();
        }
        catch (IOException e){
            System.out.println(IOExceptionMsg);
            System.exit(1);
        }

        if (!mayBeEmpty && value.isEmpty()) {
            System.out.println(stringDescription.substring(0,1).toUpperCase()+
                    stringDescription.substring(1,stringDescription.length())+
                    " cannot be empty. Try input again.");
            return readString(stringDescription, mayBeEmpty, informUser);
        }
        else{
            if (value.isEmpty()){
                throw new InputWasSkippedException(stringDescription);
            }
            return value;
        }
    }

    static public long readLong(String description, boolean mayBeEmpty, boolean informUser) {
        String stringValue = readString(description, mayBeEmpty, informUser);
        if (stringValue.isEmpty()) {
            throw new InputWasSkippedException(description);
        }
        try {
            return Long.parseLong(stringValue);
        } catch (NumberFormatException e) {
            System.out.println(description + " should be an integer value. Try input again.");
            return readLong(description, mayBeEmpty, informUser);
        }
    }

    static public int readInt(String description, boolean mayBeEmpty, boolean informUser) {
        String stringValue = readString(description, mayBeEmpty, informUser);
        if (stringValue.isEmpty()) {
            throw new InputWasSkippedException(description);
        }
        try {
            return Integer.parseInt(stringValue);
        } catch (NumberFormatException e) {
            System.out.println(description + " should be an integer value. Try input again.");
            return readInt(description, mayBeEmpty, informUser);
        }
    }

    static public int readInt(String description, int minValue, int maxValue, boolean mayBeEmpty) {
        int value = readInt(description,mayBeEmpty,mayBeEmpty);

        if (value > maxValue) {
            System.out.println(description + " should be less then " + (maxValue + 1) + ". Try input again.");
            return readInt(description, minValue, maxValue, mayBeEmpty);
        }
        if (value < minValue) {
            System.out.println(description + " should be greater then " + (minValue - 1) + ". Try input again.");
            return readInt(description, minValue, maxValue, mayBeEmpty);
        }
        return value;
    }

    static public int readInt(String description, int minValue, int maxValue) {
        return readInt(description, minValue, maxValue, false);
    }


        static public boolean askToContinue(String question){
        System.out.print(question+" (Y - yes/N - no) ");
        try {
            String s = IOUtil.consoleReader.readLine();
            if (!s.isEmpty() && (s.toLowerCase().equals("y"))){
                return true;
            }
            if (!s.isEmpty() && (s.toLowerCase().equals("n"))){
                return false;
            }
        } catch (IOException e){
            System.out.println(IOExceptionMsg);
        }
        System.out.print("You should input either \"y\" or \"n\". Try input again.\n");
        return askToContinue(question);
    }

    static public <T> void askToContinue(String question, Consumer<List<T>> consumer, List<T> param) {
        if (askToContinue(question)) {
            consumer.accept(param);
        }
    }

    static public void printCollection(String itemDescription, Collection c) {
        printCollection(itemDescription, c, true);
    }

    static public void printCollection(String itemDescription, Collection c, boolean waitForUser){
        printCollection("Existent "+itemDescription,
                "There no "+itemDescription + " yet.",
                waitForUser,
                c);
    }

    // The method prints eny collection if toString method of it's elements was automatically generated or
    // was written in a same form
    // If any exception occur it just print a list of strings generated by toString method
    static public void printCollection(String description, String msgIfEmpty,
                                           boolean waitForUser, Collection c) {
        if (c.isEmpty()) {
            System.out.println(msgIfEmpty);
            pressEnterToContinue();
        } else {
            try {
                Object obj = c.iterator().next();
                String objectToString = obj.toString();
                String[] objectToStrings = objectToString.substring(objectToString.lastIndexOf('{') + 1,
                                                                    objectToString.lastIndexOf('}'))
                                                                    .split(", ");
                final int maxColumnNumber = objectToStrings.length;
                final String[] columnNames = new String[maxColumnNumber + 1];
                int[] maxColumnLength = new int[maxColumnNumber];
                for (int j = 0; j < maxColumnNumber; j++) {
                    String currentString = objectToStrings[j];
                    columnNames[j] = currentString.substring(0, currentString.indexOf('='));
                    maxColumnLength[j] = columnNames[j].length();
                }
                String[][] table = new String[c.size()][maxColumnNumber];

                int rowNumber = 0;
                for (Object cOrder : c) {
                    String str = cOrder.toString();
                    table[rowNumber] = str.substring(str.lastIndexOf('{') + 1, str.lastIndexOf('}'))
                            .split(", ", maxColumnNumber);
                    for (int j = 0; j < maxColumnNumber; j++) {
                        String currentString = table[rowNumber][j];
                        currentString = currentString.substring(currentString.indexOf('=') + 1, currentString.length());
                        if (currentString.charAt(0) == '\'')
                            currentString = currentString.substring(1, currentString.length());
                        if (currentString.charAt(currentString.length() - 1) == '\'')
                            currentString = currentString.substring(0, currentString.length() - 1);
                        table[rowNumber][j] = currentString;
                        if (maxColumnLength[j] < table[rowNumber][j].length())
                            maxColumnLength[j] = table[rowNumber][j].length();
                    }
                    rowNumber++;
                }

                String[] formatString = new String[maxColumnNumber];
                for (int j = 0; j < maxColumnNumber; j++) {
                    formatString[j] = "%" + (maxColumnLength[j] + 3) + "s";
                }
                System.out.println(description + ":");
                System.out.print(" №");
                for (int j = 0; j < maxColumnNumber; j++) {
                    System.out.format(formatString[j], columnNames[j]);
                }
                System.out.println();
                for (int i = 0; i < c.size(); i++) {
                    System.out.format("%3d", i + 1);
                    for (int j = 0; j < maxColumnNumber; j++) {
                        System.out.format(formatString[j], table[i][j]);
                    }
                    System.out.println();
                }

            }catch (Exception e) {
                System.out.println(description + ":");
                Iterator iterator=c.iterator();
                for (int i = 0; i < c.size(); i++) {
                    System.out.format("%2d. %s\n",i+1,iterator.next().toString());
                }
            }

            if (waitForUser) pressEnterToContinue();
        }
    }

    static public void informUser(String information) {
        informUser(information, true);
    }

    static public void informUser(String information, boolean waitForUser) {
        if (waitForUser) {
            System.out.println(information);
            pressEnterToContinue();
        }else{
            System.out.println(information);
        }
    }
}
