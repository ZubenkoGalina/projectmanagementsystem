INSERT INTO skills(name) VALUES ('java'),('c++'),('sql'),('c#'),('php');

INSERT INTO companies (name) VALUES ('Luxoft'),('SoftServe');

INSERT INTO customers (name) VALUES ('TK "SAT"'),('Ltd "Horns and hooves"');

INSERT INTO projects (name, companyId, customerId, cost) VALUES
  ('Collection of horns automation',2,2,20000),
  ('Collection of hooves automation',1,2,40000),
  ('Inventory control system',2,1,60000),
  ('Logistics management',1,1,30000);

INSERT INTO developers (firstName, lastName, projectId, salary) VALUES
  ('Bolkonsky','Andrey',1,1000),
  ('Bolkonskaya','Marya',1,1500),
  ('Rostova','Natasha',2,1600),
  ('Rostov','Nikolai',2,1300),
  ('Bezukhov','Pierre',2,1200),
  ('Dolokhov','Fedor',3,1100),
  ('Kuragin','Anatole',4,1000)
;

INSERT INTO developerSkills (developerId, skillId) VALUES
  (1,1),(1,4),
  (2,3),
  (3,2),(3,5),
  (4,1),(4,2),(4,4),
  (5,4),
  (6,2),(6,3),(6,5),
  (7,1),(7,2)
;